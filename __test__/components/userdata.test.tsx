import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import UserData from "../../src/components/Userdata/userdata";

describe("UserData component", () => {
  const mockUsers = [
    {
      id: 1,
      firstName: "John",
      lastName: "Doe",
      age: 30,
      gender: "male",
      company: { department: "Marketing" },
      address: { postalCode: "12345" },
      hair: { color: "Black" },
    },
    {
      id: 2,
      firstName: "Jane",
      lastName: "Smith",
      age: 25,
      gender: "female",
      company: { department: "Sales" },
      address: { postalCode: "54321" },
      hair: { color: "Blonde" },
    },
  ];

  beforeEach(() => {
    global.fetch = jest.fn().mockResolvedValue({
      json: jest.fn().mockResolvedValue({ users: mockUsers }),
    });
  });

  test("fetches user data from API", async () => {
    render(<UserData />);
    await waitFor(() => {
      expect(screen.getByText("User Data")).toBeInTheDocument();
    });
    expect(fetch).toHaveBeenCalledWith("https://dummyjson.com/users");
  });

  test("transforms user data and groups by department", async () => {
    render(<UserData />);
    await waitFor(() => {
      expect(screen.getByText("User Data")).toBeInTheDocument();
    });

    expect(screen.getByText("Marketing")).toBeInTheDocument();
    expect(screen.getByText("Sales")).toBeInTheDocument();

    const marketingDepartment = screen
      .getByText("Marketing")
      .closest(".department");
    expect(marketingDepartment).toHaveTextContent("Male: 1");
    expect(marketingDepartment).toHaveTextContent("Female: 0");
    expect(marketingDepartment).toHaveTextContent("Age Range: 30-39");
    expect(marketingDepartment).toHaveTextContent("Black: 1");
    expect(marketingDepartment).toHaveTextContent("John Doe: 12345");

    const salesDepartment = screen.getByText("Sales").closest(".department");
    expect(salesDepartment).toHaveTextContent("Male: 0");
    expect(salesDepartment).toHaveTextContent("Female: 1");
    expect(salesDepartment).toHaveTextContent("Age Range: 20-29");
    expect(salesDepartment).toHaveTextContent("Blonde: 1");
    expect(salesDepartment).toHaveTextContent("Jane Smith: 54321");
  });
});
