import React from "react";
import { render, fireEvent, waitFor, act } from "@testing-library/react";
import ButtonList from "../../src/components/table/table";

jest.useFakeTimers();

describe("ButtonList", () => {
  test("separates items into Fruit and Vegetable lists", () => {
    const { getByText, getAllByRole } = render(<ButtonList />);

    fireEvent.click(getByText("Apple"));

    fireEvent.click(getByText("Broccoli"));

    const fruitList = getAllByRole("button", { name: "Apple" });
    const vegetableList = getAllByRole("button", { name: "Broccoli" });
    expect(fruitList).toHaveLength(1);
    expect(vegetableList).toHaveLength(1);
  });

  test("items in each category have a 5-second timer", async () => {
    const { getByText, getAllByRole } = render(<ButtonList />);


    fireEvent.click(getByText("Apple"));
    fireEvent.click(getByText("Broccoli"));

    act(() => {
      jest.advanceTimersByTime(4000);
    });


    let fruitList = getAllByRole("button", { name: "Apple" });
    let vegetableList = getAllByRole("button", { name: "Broccoli" });
    expect(fruitList).toHaveLength(1);
    expect(vegetableList).toHaveLength(1);

    act(() => {
      jest.advanceTimersByTime(1000);
    });

    await act(async () => {
      jest.runAllTimers();
    });

    await waitFor(() => {
      const mainList = getAllByRole("button", { name: /^(Apple|Broccoli)$/ });
      expect(mainList).toHaveLength(2);
    });
  });
});
