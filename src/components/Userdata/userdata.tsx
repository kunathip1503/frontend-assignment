"use client";
import React, { useEffect, useState } from "react";
import styles from "./userdata.module.css";

interface User {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
  gender: string;
  company: {
    department: string;
  };
  address: {
    postalCode: string;
  };
  hair: {
    color: string;
  };
  image: string;
}

interface DepartmentData {
  male: number;
  female: number;
  ageRange: string;
  ageAverage: number;
  hair: { [color: string]: number };
  addressUser: { [name: string]: string };
  users: User[];
}

interface GroupedDepartment {
  [department: string]: DepartmentData;
}

const UserData: React.FC = () => {
  const [groupedDepartments, setGroupedDepartments] =
    useState<GroupedDepartment>({});

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      const response = await fetch("https://dummyjson.com/users");
      const data = await response.json();
      console.log("Fetched users:", data.users);
      const groupedData = transformData(data.users);
      setGroupedDepartments(groupedData);
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const transformData = (users: User[]): GroupedDepartment => {
    const groupedData: GroupedDepartment = {};

    users.forEach((user) => {
      const { department } = user.company;
      const departmentData: DepartmentData = groupedData[department] || {
        male: 0,
        female: 0,
        ageRange: "",
        ageAverage: 0,
        hair: {},
        addressUser: {},
        users: [],
      };

      if (user.gender === "male") {
        departmentData.male++;
      } else {
        departmentData.female++;
      }

      const ageRange = getAgeRange(user.age, departmentData.ageRange);
      departmentData.ageRange = ageRange;

      departmentData.ageAverage = calculateAgeAverage(
        departmentData.users,
        user.age
      );

      const hairColor = user.hair.color;
      departmentData.hair[hairColor] =
        (departmentData.hair[hairColor] || 0) + 1;

      const fullName = `${user.firstName} ${user.lastName}`;
      departmentData.addressUser[fullName] = user.address.postalCode;

      departmentData.users.push(user);

      groupedData[department] = departmentData;
    });
    console.log('user group',groupedData)
    return groupedData;
  };

  const getAgeRange = (age: number, currentRange: string): string => {
    const rangeSize = 10;
    const minAge = Math.floor(age / rangeSize) * rangeSize;
    const maxAge = minAge + rangeSize - 1;

    if (currentRange === "") {
      return `${minAge}-${maxAge}`;
    }

    const [currentMin, currentMax] = currentRange.split("-").map(Number);
    const newMin = Math.min(currentMin, minAge);
    const newMax = Math.max(currentMax, maxAge);

    return `${newMin}-${newMax}`;
  };

  const calculateAgeAverage = (users: User[], newAge: number): number => {
    const totalAge = users.reduce((sum, user) => sum + user.age, newAge);
    const totalUsers = users.length + 1;
    return totalAge / totalUsers;
  };

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>User Data</h1>
      {Object.entries(groupedDepartments).map(([department, data]) => (
        <div key={department} className={styles.department}>
          <h2>{department}</h2>
          <p>
            <strong>Male:</strong> {data.male}
          </p>
          <p>
            <strong>Female:</strong> {data.female}
          </p>
          <p>
            <strong>Age Range:</strong> {data.ageRange} (Average Age:{" "}
            {data.ageAverage.toFixed(2)})
          </p>
          <div>
            <h3>Hair Colors</h3>
            {Object.entries(data.hair).map(([color, count]) => (
              <p key={color}>
                <strong>{color}:</strong> {count}
              </p>
            ))}
          </div>
          <div>
            <h3>Address User</h3>
            {Object.entries(data.addressUser).map(([name, postalCode]) => (
              <p key={name}>
                <strong>{name}:</strong> {postalCode}
              </p>
            ))}
          </div>
          <h3>Users</h3>
          <div className={styles.user}>
            {data.users.map((user) => (
              <div key={user.id} className={styles.userProfile}>
                <img
                  src={user.image}
                  alt={`${user.firstName} ${user.lastName}`}
                  className={styles.userImage}
                />
                <div>
                  <p>
                    <strong>Name:</strong> {user.firstName} {user.lastName}
                  </p>
                  <p>
                    <strong>Age:</strong> {user.age}
                  </p>
                  <p>
                    <strong>Gender:</strong> {user.gender}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};

export default UserData;
