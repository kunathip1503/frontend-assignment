"use client";
import { useState, useEffect } from "react";
import "../table/table.css";
import React, { FC } from "react";

interface Item {
  type: "Fruit" | "Vegetable";
  name: string;
  timer: number | null;
  clickTime: number | null;
}

const items: Item[] = [
  { type: "Fruit", name: "Apple", timer: null, clickTime: null },
  { type: "Vegetable", name: "Broccoli", timer: null, clickTime: null },
  { type: "Vegetable", name: "Mushroom", timer: null, clickTime: null },
  { type: "Fruit", name: "Banana", timer: null, clickTime: null },
  { type: "Vegetable", name: "Tomato", timer: null, clickTime: null },
  { type: "Fruit", name: "Orange", timer: null, clickTime: null },
  { type: "Fruit", name: "Mango", timer: null, clickTime: null },
  { type: "Fruit", name: "Pineapple", timer: null, clickTime: null },
  { type: "Vegetable", name: "Cucumber", timer: null, clickTime: null },
  { type: "Fruit", name: "Watermelon", timer: null, clickTime: null },
  { type: "Vegetable", name: "Carrot", timer: null, clickTime: null },
];

const ButtonList: FC = () => {
  const [mainList, setMainList] = useState<Item[]>(items);
  const [fruitList, setFruitList] = useState<Item[]>([]);
  const [vegetableList, setVegetableList] = useState<Item[]>([]);

  useEffect(() => {
    const timers = fruitList.concat(vegetableList).map((item) => ({
      ...item,
      timer:
        item.clickTime !== null
          ? setTimeout(() => {
              handleItemTimeout(item);
            }, 5000 - (Date.now() - item.clickTime))
          : null,
    }));

    return () => {
      timers.forEach((item) => item.timer && clearTimeout(item.timer));
    };
  }, [fruitList, vegetableList]);

  const handleItemTimeout = (item: Item) => {
    if (item.type === "Fruit") {
      setFruitList((prevList) =>
        prevList
          .filter((i) => i !== item)
          .sort((a, b) => a.clickTime! - b.clickTime!)
      );
    } else {
      setVegetableList((prevList) =>
        prevList
          .filter((i) => i !== item)
          .sort((a, b) => a.clickTime! - b.clickTime!)
      );
    }

    setTimeout(() => {
      setMainList((prevList) => [
        ...prevList,
        { ...item, timer: null, clickTime: null },
      ]);
    }, 500);
  };

  const handleItemClick = (item: Item, sourceList: string) => {
    if (sourceList === "mainList") {
      setMainList((prevList) => prevList.filter((i) => i !== item));
      if (item.type === "Fruit") {
        setFruitList((prevList) => [
          ...prevList,
          { ...item, timer: 5000, clickTime: Date.now() },
        ]);
      } else {
        setVegetableList((prevList) => [
          ...prevList,
          { ...item, timer: 5000, clickTime: Date.now() },
        ]);
      }
    } else if (sourceList === "fruitList") {
      setFruitList((prevList) => prevList.filter((i) => i !== item));
      setMainList((prevList) => [
        ...prevList,
        { ...item, timer: null, clickTime: null },
      ]);
    } else if (sourceList === "vegetableList") {
      setVegetableList((prevList) => prevList.filter((i) => i !== item));
      setMainList((prevList) => [
        ...prevList,
        { ...item, timer: null, clickTime: null },
      ]);
    }
  };

  return (
    <>
      <div className="container">
        <div className="button-list">
          {mainList.map((item, index) => (
            <button
              key={index}
              className="button"
              onClick={() => handleItemClick(item, "mainList")}
            >
              {item.name}
            </button>
          ))}
        </div>
        <div className="table">
          <div>
            <h2 className="texttable">Fruit</h2>
            {fruitList.map((item, index) => (
              <button
                key={index}
                className="button"
                onClick={() => handleItemClick(item, "fruitList")}
              >
                {item.name}
              </button>
            ))}
          </div>
          <div>
            <h2 className="texttable">Vegetable</h2>
            {vegetableList.map((item, index) => (
              <button
                key={index}
                className="button"
                onClick={() => handleItemClick(item, "vegetableList")}
              >
                {item.name}
              </button>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default ButtonList;
